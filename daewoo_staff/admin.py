# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from daewoo_staff.models import (
    Driver, Employee,
)

# Register your models here.

class DriverAdmin(admin.ModelAdmin):
    list_display = (
        '__unicode__', 'name', 'father_name', 'cnic', 'address', 'mobile', 'joinning_date', 'salary', 'daewoo',
    )
    @staticmethod
    def daewoo(obj):
        return obj.daewoo.name
    search_fields = (
        'name', 'cnic', 'mobile', 'driver__daewoo__plate_no',
    )

class EmployeeAdmin(admin.ModelAdmin):
    list_display = (
        '__unicode__', 'designation', 'name', 'father_name', 'cnic', 'address', 'mobile', 'joinning_date', 'salary',
    )

    search_fields = (
        'name', 'cnic', 'mobile', 'designation',
    )

admin.site.register(Driver, DriverAdmin)
admin.site.register(Employee, EmployeeAdmin)
