from django import forms
from daewoo_staff.models import (
    Driver, Employee,
)

class DriverForm(forms.ModelForm):
    class Meta:
        model = Driver
        fields = '__all__'

class EmployeeForm(forms.ModelForm):
    class Meta:
        model = Employee
        fields = '__all__'