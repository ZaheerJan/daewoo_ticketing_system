from __future__ import unicode_literals

from django.shortcuts import render
from django.views.generic import TemplateView
from django.views.generic import FormView, UpdateView, DeleteView
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse, reverse_lazy
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from daewoo_staff.models import (
    Driver, Employee,
)
from daewoo_staff.forms import (
    DriverForm, EmployeeForm,
)
from daewoo_vehicle.models import Daewoo


class DriverList(TemplateView):
    template_name = 'staff/list_driver.html'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect(reverse('common:login'))
        elif (
                    self.request.user.user_profile.user_type ==
                    self.request.user.user_profile.USER_TYPE_MUNSHI
        ):
            return HttpResponseRedirect(
                reverse('booking:create_schedule')
            )
        else:
            return super(
                DriverList, self).dispatch(request, *args, **kwargs)

    @staticmethod
    def get_driver():
        """
        returns all drivers
        :return:
        """
        return Driver.objects.all()

    def get_context_data(self, **kwargs):
        context = super(DriverList, self).get_context_data(**kwargs)

        context.update({
            'drivers': self.get_driver()
        })
        return context


class AddDriver(FormView):
    form_class = DriverForm
    template_name = 'staff/add_driver.html'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect(reverse('common:login'))
        elif (
                    self.request.user.user_profile.user_type ==
                    self.request.user.user_profile.USER_TYPE_MUNSHI
        ):
            return HttpResponseRedirect(
                reverse('booking:create_schedule')
            )
        else:
            return super(
                AddDriver, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AddDriver, self).get_context_data(**kwargs)
        daewoo= (
            Daewoo.objects.all()
        )

        context.update(
            {
                'daewoo':daewoo
            }
        )
        return context

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.daewoo=Daewoo.objects.get(plate_no=self.request.POST.get('plate_no'))
        obj.save()
        return HttpResponseRedirect(reverse('staff:driver_list'))

    def form_invalid(self, form):
        return HttpResponseRedirect(reverse('staff:add_driver'))


class DriverDeleteView(DeleteView):
    model = Driver
    success_url = reverse_lazy('staff:driver_list')
    success_message = "Delete vehicle Successfully"

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


class DriverUpdateView(UpdateView):
    form_class = DriverForm
    template_name = 'staff/update_driver.html'
    model = Driver
    success_url = reverse_lazy('staff:driver_list')

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect(reverse('common:login'))
        elif (
                    self.request.user.user_profile.user_type ==
                    self.request.user.user_profile.USER_TYPE_MUNSHI
        ):
            return HttpResponseRedirect(
                reverse('booking:create_schedule')
            )
        else:
            return super(
                DriverUpdateView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.daewoo=Daewoo.objects.get(plate_no=self.request.POST.get('plate_no'))
        obj.save()
        return HttpResponseRedirect(reverse('staff:driver_list'))

    def form_invalid(self, form):
        return super(DriverUpdateView, self).form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(DriverUpdateView, self).get_context_data(**kwargs)
        daewoo= (
            Daewoo.objects.all()
        )

        context.update(
            {
                'daewoo':daewoo
            }
        )
        return context


class EmployeeList(TemplateView):
    template_name = "staff/list_employee.html"

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect(reverse('common:login'))
        elif (
                    self.request.user.user_profile.user_type ==
                    self.request.user.user_profile.USER_TYPE_MUNSHI
        ):
            return HttpResponseRedirect(
                reverse('booking:create_schedule')
            )
        else:
            return super(
                EmployeeList, self).dispatch(request, *args, **kwargs)

    @staticmethod
    def get_employee():
        """"
        returns all employess
        :return:
        """
        return Employee.objects.all()

    def get_context_data(self, **kwargs):
        context = super(EmployeeList,self).get_context_data(**kwargs)

        context.update({
            'employees':self.get_employee()
        })
        return context


class AddEmployee(FormView):
    form_class = EmployeeForm
    template_name = "staff/add_employee.html"

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect(reverse('common:login'))
        elif (
                    self.request.user.user_profile.user_type ==
                    self.request.user.user_profile.USER_TYPE_MUNSHI
        ):
            return HttpResponseRedirect(
                reverse('booking:create_schedule')
            )
        else:
            return super(
                AddEmployee, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AddEmployee, self).get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        employee= form.save()
        return HttpResponseRedirect(reverse('staff:list_employee'))

    def form_invalid(self, form):
        return HttpResponseRedirect(reverse('staff:add_employee'))


class EmployeeDeleteView(DeleteView):
    model = Employee
    success_url = reverse_lazy('staff:list_employee')
    success_message = "Deleted Employee Successfully"

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


class EmployeeUpdateView(UpdateView):
    form_class = EmployeeForm
    model = Employee
    template_name = "staff/update_employee.html"
    success_url = reverse_lazy('staff:list_employee')

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect(reverse('common:login'))
        elif (
                    self.request.user.user_profile.user_type ==
                    self.request.user.user_profile.USER_TYPE_MUNSHI
        ):
            return HttpResponseRedirect(
                reverse('booking:create_schedule')
            )
        else:
            return super(
                EmployeeUpdateView,self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        employee= form.save()
        return HttpResponseRedirect(reverse('staff:list_employee'))

    def form_invalid(self, form):
        return HttpResponseRedirect(reverse('staff:add_employee'))