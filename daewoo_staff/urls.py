from django.conf.urls import url, include
from daewoo_staff.views import (
    AddDriver, DriverList, DriverDeleteView, DriverUpdateView, AddEmployee, EmployeeList, EmployeeDeleteView, EmployeeUpdateView,

)

urlpatterns = [
    url(
        r'^add/$',
        AddDriver.as_view(),
        name='add_driver'
    ),

    url(
        r'^list/$',
        DriverList.as_view(),
        name='driver_list'
    ),

    url(
        r'^delete/(?P<pk>\d+)/$',
        DriverDeleteView.as_view(),
        name='driver_delete'
    ),

    url(
        r'^update/(?P<pk>\d+)/$',
        DriverUpdateView.as_view(),
        name='driver_update'
    ),
########################################## Employee YRLS ###################################
    url(
        r'^addemployee/$',
        AddEmployee.as_view(),
        name='add_employee'
    ),

    url(
        r'^listemployee/$',
        EmployeeList.as_view(),
        name='list_employee'
    ),

    url(
        r'^deleteemployee/(?P<pk>\d+)/$',
        EmployeeDeleteView.as_view(),
        name='delete_employee'
    ),

    url(
        r'^updateemployee/(?P<pk>\d+)/$',
        EmployeeUpdateView.as_view(),
        name='update_employee'
    ),
]
