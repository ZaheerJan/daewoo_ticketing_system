# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


class Driver(models.Model):
    name = models.CharField(max_length=100)
    father_name = models.CharField(max_length=100, blank=True, null=True)
    cnic = models.CharField(max_length=100)
    address = models.TextField(max_length=500, blank=True, null=True)
    mobile = models.CharField(max_length=100, blank=True, null=True)
    joinning_date = models.CharField(max_length=100, blank=True, null=True)
    salary = models.CharField(max_length=100, blank=True, null=True)
    daewoo = models.ForeignKey(
        'daewoo_vehicle.Daewoo', blank=True, null=True,
        related_name='daewoo_driver'
    )

    def __unicode__(self):
        return self.name


class Employee(models.Model):
    designation = models.CharField(max_length=100, blank=True, null=True)
    name = models.CharField(max_length=100)
    father_name = models.CharField(max_length=100, blank=True, null=True)
    cnic = models.CharField(max_length=100)
    address = models.TextField(max_length=500, blank=True, null=True)
    mobile = models.CharField(max_length=100, blank=True, null=True)
    joinning_date = models.CharField(max_length=100, blank=True, null=True)
    salary = models.CharField(max_length=100, blank=True, null=True)


    def __unicode__(self):
        return self.name

