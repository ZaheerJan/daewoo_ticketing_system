# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class DaewooStaffConfig(AppConfig):
    name = 'daewoo_staff'
