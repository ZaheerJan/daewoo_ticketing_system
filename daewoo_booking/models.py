# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

from django.utils import timezone


class Passenger(models.Model):
    name = models.CharField(max_length=200)
    cnic = models.CharField(max_length=20, blank=True, null=True)
    passport_no = models.CharField(max_length=20, blank=True, null=True)
    immigrant_card_no = models.CharField(max_length=20, blank=True, null=True)
    father_name = models.CharField(max_length=200, blank=True, null=True)
    mobile_no = models.CharField(max_length=20, blank=True, null=True)
    address = models.TextField(
        max_length=500, blank=True, null=True
    )

    def __unicode__(self):
        return '%s' % self.name


class StationReservation(models.Model):
    name = models.CharField(max_length=100)
    mobile_no = models.CharField(max_length=20, blank=True, null=True)
    station_name = models.CharField(max_length=100, blank=True, null=True)

    def __unicode__(self):
        return self.name


class Booking(models.Model):
    ticket_no = models.CharField(
        max_length=10, blank=True, null=True
    )
    passenger = models.ForeignKey(
        Passenger, related_name='passenger_booking', blank=True, null=True
    )
    station_reservation = models.ForeignKey(
        StationReservation, related_name='station_reservation_booking',
        blank=True, null=True
    )
    booking_date = models.DateField(
        default=timezone.now, blank=True, null=True
    )
    departure = models.DateField(
        blank=True, null=True
    )
    route = models.ForeignKey(
        'daewoo_route.Route', related_name='booking_route',
        blank=True, null=True
    )
    destination = models.ForeignKey(
        'daewoo_route.Destination', related_name='booking_destination',
        blank=True, null=True
    )
    station = models.ForeignKey(
        'daewoo_route.Stations', related_name='booking_station',
        blank=True, null=True
    )
    departure_timing = models.ForeignKey(
        'daewoo_route.DepartureTiming',
        related_name='booking_departure_timing',
        blank=True, null=True
    )
    seats_no = models.CharField(max_length=200, blank=True, null=True)
    seats_quantity = models.IntegerField(blank=True, null=True)
    total_amount = models.DecimalField(
        max_digits=65, decimal_places=2, default=0, blank=True, null=True,
    )
    discount = models.DecimalField(
        max_digits=65, decimal_places=2, default=0, blank=True, null=True,
    )
    reference = models.CharField(
        max_length=100, blank=True, null=True
    )
    has_payment = models.BooleanField(
        default=True
    )

    def __unicode__(self):
        return '%s' % (
            self.passenger.name
            if self.passenger else
            self.station_reservation.name
        )

    def grand_total(self):
        return  self.total_amount - self.discount
