from django import forms

from .models import Passenger, Booking, StationReservation


class PassengerForm(forms.ModelForm):
    class Meta:
        fields = '__all__'
        model = Passenger


class BookingForm(forms.ModelForm):
    class Meta:
        fields = '__all__'
        model = Booking

class StationReservationForm(forms.ModelForm):
    class Meta:
        fields = '__all__'
        model = StationReservation