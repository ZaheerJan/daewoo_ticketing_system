from django.http import JsonResponse, HttpResponseRedirect
from django.utils import timezone
from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt
from django.db import transaction
from django.core.urlresolvers import reverse

from daewoo_booking.forms import PassengerForm, BookingForm
from daewoo_booking.models import Passenger, Booking, StationReservation
from common.models import UserTickerRecords


class CreateBookingAPIView(View):

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(
            CreateBookingAPIView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        route_id = self.request.POST.get('route_id')
        destination_id = self.request.POST.get('destination_id')
        last_station = self.request.POST.get('station_id')
        vehicle_id = self.request.POST.get('vehicle_id')
        departure_id = self.request.POST.get('departure_id')
        booking_date = self.request.POST.get('booking_date')

        customer_cnic = self.request.POST.get('customer_cnic')
        customer_name = self.request.POST.get('customer_name')
        customer_mobile = self.request.POST.get('customer_mobile')
        passport_no = self.request.POST.get('passport_no')
        muhajir_card_no = self.request.POST.get('muhajir_card_no')

        booking_fares = self.request.POST.get('fares')
        seats_no = self.request.POST.get('seats_no')
        quantity = self.request.POST.get('quantity')
        total_amount = self.request.POST.get('total_amount')
        discount = self.request.POST.get('discount')
        reference = self.request.POST.get('reference')
        payment = self.request.POST.get('payment')

        if payment == 'Paid':
            has_payment = True
        else:
            has_payment = False

        with transaction.atomic():
            booking_form_kwargs = {
                'booking_date': timezone.now().date(),
                'departure_timing': departure_id,
                'route': route_id,
                'destination': destination_id,
                'station': last_station,
                'departure': booking_date,
                'seats_no': seats_no,
                'seats_quantity': quantity,
                'total_amount': total_amount,
                'discount': discount,
                'reference': reference,
                'has_payment': has_payment

            }
            if self.request.POST.get('reservation_type') == 'Passenger Booking':

                try:
                    if customer_cnic:
                        passenger = Passenger.objects.get(cnic=customer_cnic)
                    elif passport_no:
                        passenger = Passenger.objects.get(passport_no=passport_no)
                    else:
                        passenger = Passenger.objects.get(
                            immigrant_card_no=muhajir_card_no)
                    booking_form_kwargs.update({
                        'passenger': passenger.id
                    })
                except:
                    passenger_form_kwargs = {
                        'cnic': customer_cnic,
                        'name': customer_name,
                        'mobile_no': customer_mobile,
                        'passport_no': passport_no,
                        'immigrant_card_no': muhajir_card_no,
                    }

                    passenger_form = PassengerForm(passenger_form_kwargs)

                    if passenger_form.is_valid():
                        passenger = passenger_form.save()
                        booking_form_kwargs.update({
                            'passenger': passenger.id
                        })

                    else:
                        return JsonResponse({
                            'success': False,
                            'message': 'Customer details are required'
                        })
            else:
                station_reservation = StationReservation.objects.get(
                    name=self.request.POST.get('station_booking')
                )
                booking_form_kwargs.update({
                    'station_reservation': station_reservation.id
                })

            booking_form = BookingForm(booking_form_kwargs)
            if booking_form.is_valid():
                booking = booking_form.save()
            else:
                return JsonResponse({
                    'success': False,
                    'message': 'Booking details are wrong'
                })

            print total_amount
            print discount
            print has_payment

            print "---------------coming here----------"
            print "---------------coming here----------"
            print "---------------coming here----------"

            if discount:
                total_amount = float(total_amount) - float(discount)

            if not has_payment:
                total_amount = 0

            UserTickerRecords.objects.create(
                user=self.request.user,
                ticket_quantity=quantity,
                ticket_amount=total_amount,
                ticket_status=UserTickerRecords.STATUS_RESERVED,
                record_date=timezone.now()
            )

            return JsonResponse({
                'success': True,
                'message': 'Working Successfully',
                'booking_id': booking.id
            })


class MakeBookingPaymentAPIView(View):

    def get(self, request, *args, **kwargs):
        booking_id = self.kwargs.get('booking_id')
        booking = Booking.objects.get(id=booking_id)
        booking.has_payment = True
        booking.save()

        return HttpResponseRedirect(
            reverse('booking:booking_list', kwargs={
                'destination_id': booking.destination.id,
                'timing_id': booking.departure_timing.id,
                'date': booking.departure.strftime('%Y-%m-%d')
            })
        )
