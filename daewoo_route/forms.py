from django import forms
from daewoo_route.models import (
    Route, Stations, Destination, DepartureTiming,
)

class RouteForm(forms.ModelForm):
    class Meta:
        model = Route
        fields = '__all__'

class StationsForm(forms.ModelForm):
    class Meta:
        model = Stations
        fields = '__all__'

class DestinationForm(forms.ModelForm):
    class Meta:
        model = Destination
        fields = '__all__'

class DepartureTimingForm(forms.ModelForm):
    class Meta:
        model = DepartureTiming
        fields = '__all__'