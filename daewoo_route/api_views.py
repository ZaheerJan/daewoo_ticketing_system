from django.http import JsonResponse
from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt

from daewoo_route.models import Destination, Stations, DepartureTiming


class DepartureTimingAPIView(View):

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(
            DepartureTimingAPIView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        destination = Destination.objects.get(
            name=self.request.POST.get('destination')
        )
        departure_timings = destination.destination_timing.all().order_by('timing')
        stations = destination.destination_station.all().order_by('name')
        timings = [
            {
                'id': t.id, 'timing': t.timing,
            }
            for t in departure_timings
        ]

        stations = [
            {'id': s.id, 'station': s.name}
            for s in stations
        ]

        return JsonResponse({
            'departure_timings': timings,
            'stations': stations
        })


class StationsAPIView(View):

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(
            StationsAPIView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        destination = Destination.objects.get(
            name=self.request.POST.get('destination')
        )
        departure_timing = DepartureTiming.objects.get(
            id=self.request.POST.get('departure_timing'))

        stations = destination.destination_station.filter(
            daewoo_type=departure_timing.daewoo_type)
        stations = [
            {'id': s.id, 'station': s.name}
            for s in stations
        ]
        return JsonResponse(
            {
                'stations': stations
            }
        )