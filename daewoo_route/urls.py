from django.conf.urls import url, include
from daewoo_route.views import (
    AddRoute, RouteList, RouteUpdateView, RouteDeleteView, DestinationList, DestinationUpdateView, AddDestination,
    DestinationDeleteView, AddStation, StationsList, StationDeleteView, StationUpdateView, AddDepartureTiming,
    DepartureTimingList, DepartureTimingUpdateView, DepartureTimingDeleteView,
)

from daewoo_route.api_views import DepartureTimingAPIView, StationsAPIView

urlpatterns = [
    url(
        r'^add/$',
        AddRoute.as_view(),
        name='add_route'
    ),

    url(
        r'^list/$',
        RouteList.as_view(),
        name='list_route'
    ),

    url(
        r'^delete/(?P<pk>\d+)/$',
        RouteDeleteView.as_view(),
        name='delete_route'
    ),

    url(
        r'^update/(?P<pk>\d+)/$',
        RouteUpdateView.as_view(),
        name='update_route'
    ),

    ############################ Destination URLS ##################################

    url(
        r'^adddestination/$',
        AddDestination.as_view(),
        name='add_destination'
    ),

    url(
        r'^listdestination/$',
        DestinationList.as_view(),
        name='list_destination'
    ),

    url(
        r'^deletedestination/(?P<pk>\d+)/$',
        DestinationDeleteView.as_view(),
        name='delete_destination'
    ),

    url(
        r'^updatedestination/(?P<pk>\d+)/$',
        DestinationUpdateView.as_view(),
        name='update_destination'
    ),

    ###########################  Stations URLS ######################################

    url(
        r'^addstaion/$',
        AddStation.as_view(),
        name='add_station'
    ),

    url(
        r'^liststation/$',
        StationsList.as_view(),
        name='list_station'
    ),

    url(
        r'^deletestation/(?P<pk>\d+)/$',
        StationDeleteView.as_view(),
        name='delete_station'
    ),

    url(
        r'^updatestation/(?P<pk>\d+)/$',
        StationUpdateView.as_view(),
        name='update_station'
    ),

    ######################################## Departure Timing Urls #####################################

    url(
        r'^adddeparture/$',
        AddDepartureTiming.as_view(),
        name='add_departure'
    ),

    url(
        r'^listdeparture/$',
        DepartureTimingList.as_view(),
        name='list_departure'
    ),

    url(
        r'^deletedeparture/(?P<pk>\d+)/$',
        DepartureTimingDeleteView.as_view(),
        name='delete_departure'
    ),

    url(
        r'^updatedeparture/(?P<pk>\d+)/$',
        DepartureTimingUpdateView.as_view(),
        name='update_departure'
    ),

    ########################################  Route Timings URLS #######################################
    url(
        r'^departure/timing/api/$',
        DepartureTimingAPIView.as_view(),
        name='departure_timing_api'
    ),

    url(
        r'^stations/api/$',
        StationsAPIView.as_view(),
        name='stations_api'
    ),
]
