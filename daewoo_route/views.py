# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.generic import TemplateView
from django.views.generic import FormView, UpdateView, DeleteView
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse, reverse_lazy
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from daewoo_vehicle.models import Daewoo
from daewoo_route.models import (
    Route, Stations, RouteTimings, Destination, DepartureTiming,
)

from daewoo_route.forms import (
    RouteForm, StationsForm, DestinationForm, DepartureTimingForm,
)


class RouteList(TemplateView):
    template_name = 'route/list_route.html'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect(reverse('common:login'))
        elif (
                    self.request.user.user_profile.user_type ==
                    self.request.user.user_profile.USER_TYPE_MUNSHI
        ):
            return HttpResponseRedirect(
                reverse('booking:create_schedule')
            )
        else:
            return super(
                RouteList, self).dispatch(request, *args, **kwargs)

    @staticmethod
    def get_route():
        """
        returns all routes
        :return:
        """
        return Route.objects.all()

    def get_context_data(self, **kwargs):
        context = super(RouteList, self).get_context_data(**kwargs)

        context.update({
            'routes': self.get_route()
        })
        return context


class AddRoute(FormView):
    form_class = RouteForm
    template_name = 'route/add_route.html'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect(reverse('common:login'))
        elif (
                    self.request.user.user_profile.user_type ==
                    self.request.user.user_profile.USER_TYPE_MUNSHI
        ):
            return HttpResponseRedirect(
                reverse('booking:create_schedule')
            )
        else:
            return super(
                AddRoute,self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AddRoute, self).get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        route = form.save()
        return HttpResponseRedirect(reverse('route:list_route'))

    def form_invalid(self, form):
        return HttpResponseRedirect(reverse('route:add_route'))


class RouteDeleteView(DeleteView):
    model = Route
    success_url = reverse_lazy('route:list_route')
    success_message = "Delete vehicle Successfully"

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


class RouteUpdateView(UpdateView):
    form_class = RouteForm
    template_name = 'route/update_route.html'
    model = Route
    success_url = reverse_lazy('route:list_route')

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect(reverse('common:login'))
        elif (
                    self.request.user.user_profile.user_type ==
                    self.request.user.user_profile.USER_TYPE_MUNSHI
        ):
            return HttpResponseRedirect(
                reverse('booking:create_schedule')
            )
        else:
            return super(
                RouteUpdateView, self).dispatch(request, *args, **kwargs)


class DestinationList(TemplateView):
    template_name = 'destination/list_destination.html'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect(reverse('common:login'))
        elif (
                    self.request.user.user_profile.user_type ==
                    self.request.user.user_profile.USER_TYPE_MUNSHI
        ):
            return HttpResponseRedirect(
                reverse('booking:create_schedule')
            )
        else:
            return super(
                DestinationList,self).dispatch(request, *args, **kwargs)

    @staticmethod
    def get_destination():
        """
        returns all destinations
        :return:
        """
        return Destination.objects.all()

    def get_context_data(self, **kwargs):
        context = super(DestinationList, self).get_context_data(**kwargs)

        context.update({
            'destinations': self.get_destination()
        })
        return context


class AddDestination(FormView):
    form_class = DestinationForm
    template_name = 'destination/add_destination.html'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect(reverse('common:login'))
        elif (
                    self.request.user.user_profile.user_type ==
                    self.request.user.user_profile.USER_TYPE_MUNSHI
        ):
            return HttpResponseRedirect(
                reverse('booking:create_schedule')
            )
        else:
            return super(
                AddDestination,self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AddDestination, self).get_context_data(**kwargs)
        route = (
            Route.objects.all()
        )

        context.update(
            {
                'route':route
            }
        )
        return context

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.route=Route.objects.get(name=self.request.POST.get('name1'))
        obj.save()
        return HttpResponseRedirect(reverse('route:list_destination'))

    def form_invalid(self, form):
        return HttpResponseRedirect(reverse('route:add_destination'))


class DestinationDeleteView(DeleteView):
    model = Destination
    success_url = reverse_lazy('route:list_destination')
    success_message = "Delete vehicle Successfully"

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


class DestinationUpdateView(UpdateView):
    form_class = DestinationForm
    template_name = 'destination/update_destination.html'
    model = Destination
    success_url = reverse_lazy('route:list_destination')

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect(reverse('common:login'))
        elif (
                    self.request.user.user_profile.user_type ==
                    self.request.user.user_profile.USER_TYPE_MUNSHI
        ):
            return HttpResponseRedirect(
                reverse('booking:create_schedule')
            )
        else:
            return super(
                DestinationUpdateView,self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.route = Route.objects.get(name=self.request.POST.get('name1'))
        obj.save()
        return HttpResponseRedirect(reverse('route:list_destination'))

    def form_invalid(self, form):
        return super(DestinationUpdateView, self).form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(DestinationUpdateView, self).get_context_data(**kwargs)
        route= (
            Route.objects.all()
        )

        context.update(
            {
                'routes':route
            }
        )
        return context


class StationsList(TemplateView):
    template_name = 'stations/list_stations.html'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect(reverse('common:login'))
        elif (
                    self.request.user.user_profile.user_type ==
                    self.request.user.user_profile.USER_TYPE_MUNSHI
        ):
            return HttpResponseRedirect(
                reverse('booking:create_schedule')
            )
        else:
            return super(
                StationsList,self).dispatch(request, *args, **kwargs)

    @staticmethod
    def get_station():
        """
        returns all stations
        :return:
        """
        return Stations.objects.all()

    def get_context_data(self, **kwargs):
        context = super(StationsList, self).get_context_data(**kwargs)

        context.update({
            'stations': self.get_station()
        })
        return context


class AddStation(FormView):
    form_class = StationsForm
    template_name = 'stations/add_stations.html'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect(reverse('common:login'))
        elif (
                    self.request.user.user_profile.user_type ==
                    self.request.user.user_profile.USER_TYPE_MUNSHI
        ):
            return HttpResponseRedirect(
                reverse('booking:create_schedule')
            )
        else:
            return super(
                AddStation,self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AddStation, self).get_context_data(**kwargs)
        destination = (
            Destination.objects.all()
        )

        context.update(
            {
                'destinations':destination
            }
        )
        return context

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.destination=Destination.objects.get(name=self.request.POST.get('name1'))
        obj.save()
        return HttpResponseRedirect(reverse('route:list_station'))

    def form_invalid(self, form):
        return HttpResponseRedirect(reverse('route:add_station'))


class StationDeleteView(DeleteView):
    model = Stations
    success_url = reverse_lazy('route:list_station')
    success_message = "Delete vehicle Successfully"

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


class StationUpdateView(UpdateView):
    form_class = StationsForm
    template_name = 'stations/update_stations.html'
    model = Stations
    success_url = reverse_lazy('route:list_station')

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect(reverse('common:login'))
        elif (
                    self.request.user.user_profile.user_type ==
                    self.request.user.user_profile.USER_TYPE_MUNSHI
        ):
            return HttpResponseRedirect(
                reverse('booking:create_schedule')
            )
        else:
            return super(
                StationUpdateView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.destination = Destination.objects.get(name=self.request.POST.get('name1'))
        obj.save()
        return HttpResponseRedirect(reverse('route:list_station'))

    def form_invalid(self, form):
        return super(StationUpdateView, self).form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(StationUpdateView, self).get_context_data(**kwargs)
        destination= (
            Destination.objects.all()
        )

        context.update(
            {
                'destinations':destination
            }
        )
        return context


class DepartureTimingList(TemplateView):
    template_name = 'timing/list_timing.html'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect(reverse('common:login'))
        elif (
                    self.request.user.user_profile.user_type ==
                    self.request.user.user_profile.USER_TYPE_MUNSHI
        ):
            return HttpResponseRedirect(
                reverse('booking:create_schedule')
            )
        else:
            return super(
                DepartureTimingList, self).dispatch(request, *args, **kwargs)

    @staticmethod
    def get_departuretiming():
        """
        returns all departuretimings
        :return:
        """
        return DepartureTiming.objects.all()

    def get_context_data(self, **kwargs):
        context = super(DepartureTimingList, self).get_context_data(**kwargs)

        context.update({
            'timings': self.get_departuretiming()
        })
        return context


class AddDepartureTiming(FormView):
    form_class = DepartureTimingForm
    template_name = 'timing/add_timing.html'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect(reverse('common:login'))
        elif (
                    self.request.user.user_profile.user_type ==
                    self.request.user.user_profile.USER_TYPE_MUNSHI
        ):
            return HttpResponseRedirect(
                reverse('booking:create_schedule')
            )
        else:
            return super(
                AddDepartureTiming, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AddDepartureTiming, self).get_context_data(**kwargs)
        destination = (
            Destination.objects.all()
        )

        context.update(
            {
                'destinations':destination,
            }
        )
        return context

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.destination=Destination.objects.get(name=self.request.POST.get('name'))

        obj.save()
        return HttpResponseRedirect(reverse('route:list_departure'))

    def form_invalid(self, form):
        return HttpResponseRedirect(reverse('route:add_departure'))


class DepartureTimingDeleteView(DeleteView):
    model = DepartureTiming
    success_url = reverse_lazy('route:list_departure')
    success_message = "Delete vehicle Successfully"

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


class DepartureTimingUpdateView(UpdateView):
    form_class = DepartureTimingForm
    template_name = 'timing/update_timing.html'
    model = DepartureTiming
    success_url = reverse_lazy('route:list_departure')

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect(reverse('common:login'))
        elif (
                    self.request.user.user_profile.user_type ==
                    self.request.user.user_profile.USER_TYPE_MUNSHI
        ):
            return HttpResponseRedirect(
                reverse('booking:create_schedule')
            )
        else:
            return super(
                DepartureTimingUpdateView, self
            ).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DepartureTimingUpdateView, self).get_context_data(**kwargs)
        destination = (
            Destination.objects.all()
        )

        context.update(
            {
                'destinations':destination,
            }
        )
        return context

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.destination=Destination.objects.get(name=self.request.POST.get('name'))

        obj.save()
        return HttpResponseRedirect(reverse('route:list_departure'))

    def form_invalid(self, form):
        return HttpResponseRedirect(reverse('#'))
