# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from daewoo_route.models import (
    Route, Stations, Destination, DepartureTiming, RouteTimings, RouteUser
)


class RouteAdmin(admin.ModelAdmin):
    list_display = (
        '__unicode__', 'name', 'via', 'to',
    )
    search_fields = (
        'name', 'via', 'to',
    )


class RouteUserAdmin(admin.ModelAdmin):
    list_display = (
        '__unicode__', 'route'
    )
    raw_id_fields = (
        'user', 'route',
    )


class DestinationAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'route',
    )
    search_fields = (
        'name', 'route__name'
    )

    @staticmethod
    def route(obj):
        return obj.route.name


class StationsAdmin(admin.ModelAdmin):
    list_display = (
        '__unicode__', 'name', 'fare', 'destination', 'daewoo_type'
    )
    search_fields = (
         'name', 'fare'
    )
    @staticmethod
    def destination(obj):
        return obj.destination.name


class DepartureTimingAdmin(admin.ModelAdmin):
    list_display = (
        'destination', 'timing', 'vehicle',
    )
    search_fields = (
                        'destination__name',
    )

    @staticmethod
    def destination(obj):
        return obj.destination.name

    @staticmethod
    def vehicle(obj):
        return obj.vehicle.plate_no


class RouteTimingsAdmin(admin.ModelAdmin):
    list_display = (
        'station', 'time',
    )
    @staticmethod
    def station(obj):
        return obj.station.name

    search_fields = (
        'station__name', 'time',
    )


admin.site.register(Route, RouteAdmin)
admin.site.register(Stations, StationsAdmin)
admin.site.register(Destination, DestinationAdmin)
admin.site.register(DepartureTiming, DepartureTimingAdmin)
admin.site.register(RouteUser, RouteUserAdmin)
admin.site.register(RouteTimings, RouteTimingsAdmin)
