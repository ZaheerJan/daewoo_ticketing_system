# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from daewoo_vehicle.models import Daewoo


class DaewooAdmin(admin.ModelAdmin):
    list_display = (
        '__unicode__', 'name', 'plate_no', 'vip_seat', 'total_seats',
    )
    search_fields = (
        'name', 'plate_no',
    )


admin.site.register(Daewoo, DaewooAdmin)
