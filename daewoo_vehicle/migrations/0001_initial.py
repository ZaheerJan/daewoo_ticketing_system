# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2018-12-04 08:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Daewoo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('plate_no', models.CharField(max_length=100)),
                ('vipseats', models.BooleanField(default='false')),
                ('totalseats', models.IntegerField(max_length=100)),
            ],
        ),
    ]
