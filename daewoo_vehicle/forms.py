from django import forms
from django.views.generic import TemplateView
from daewoo_vehicle.models import Daewoo


class DaewooForm(forms.ModelForm):
    class Meta:
        model = Daewoo
        fields = '__all__'