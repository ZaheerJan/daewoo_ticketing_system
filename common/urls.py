from django.conf.urls import url, include
from common.views import (
    LoginView, LogoutView, HomePageView, RegisterView, UserDeleteView,
    UserProfileList, UserTicketRecordsView,InchargePageView, DailyReportsView,
    MonthlyReportsView
)

urlpatterns = [

    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
    url(r'^$', HomePageView.as_view(), name='index'),
    url(r'^incharge/$', InchargePageView.as_view(), name='index2'),
    url(r'^register/$', RegisterView.as_view(), name='register'),
    url(
        r'^userlist/$',
        UserProfileList.as_view(),
        name='list_user'
    ),

    url(
        r'^user/delete/(?P<pk>\d+)/$',
        UserDeleteView.as_view(),
        name='delete_user'
    ),

    url(
        r'^user/(?P<user_id>\d+)/record/$',
        UserTicketRecordsView.as_view(),
        name='user_records'
    ),
    url(
        r'^user/(?P<user_id>\d+)/record/(?P<date>[a-zA-Z0-9_-]+)/$',
        UserTicketRecordsView.as_view(),
        name='user_records_date'
    ),
    url(
        r'^daily/reports/$',
        DailyReportsView.as_view(),
        name='daily_reports'
    ),
    url(
        r'^monthly/reports/$',
        MonthlyReportsView.as_view(),
        name='monthly_reports'
    ),
]
