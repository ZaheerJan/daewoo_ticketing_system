# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime

from django.contrib.auth import forms as auth_forms
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.contrib.auth import authenticate
from django.views.generic import TemplateView, RedirectView, UpdateView, DeleteView
from django.views.generic import FormView
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse, reverse_lazy
from django.db import transaction
from django.db.models import Sum
from django.utils import timezone
from django.contrib.auth.models import User
from django.conf import settings
from django.core.paginator import Paginator
from dateutil.relativedelta import relativedelta
from calendar import monthrange

from common.models import UserProfile, UserTickerRecords
from daewoo_route.models import Route, RouteUser, DepartureTiming
from daewoo_booking.models import Booking


class UserProfileList(TemplateView):
    template_name = 'user_profiles.html'

    @staticmethod
    def get_userprofile():
        """
        returns all userprofiles
        :return:
        """
        return UserProfile.objects.all()

    def get_context_data(self, **kwargs):
        context = super(UserProfileList, self).get_context_data(**kwargs)
        users = self.get_userprofile().order_by('user__username')
        if self.request.user.user_profile.user_type == self.request.user.user_profile.USER_TYPE_INCHARGE:
            route_users = RouteUser.objects.filter(route__id=self.request.user.user_route.route.id)
            users = users.filter(user_id__in=[u.user.id for u in route_users])

        context.update({
            'users': users
        })
        return context


class UserDeleteView(DeleteView):
    model = UserProfile
    success_url = reverse_lazy('common:list_user')
    success_message = "Deleted Employee Successfully"

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


class LoginView(FormView):
    template_name = 'login.html'
    form_class = auth_forms.AuthenticationForm

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_authenticated():
            if (
                self.request.user.user_profile.user_type ==
                self.request.user.user_profile.USER_TYPE_MUNSHI
            ):
                return HttpResponseRedirect(
                    reverse('booking:create_schedule')
                )
            elif(
                self.request.user.user_profile.user_type ==
                self.request.user.user_profile.USER_TYPE_INCHARGE
            ):
                return HttpResponseRedirect(
                    reverse('common:index2')
                )
            else:
                return HttpResponseRedirect(
                    reverse('common:index')
                )

        return super(LoginView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        user = form.get_user()
        auth_login(self.request, user)
        if (
            self.request.user.user_profile.user_type ==
            self.request.user.user_profile.USER_TYPE_MUNSHI
        ):
            return HttpResponseRedirect(
                reverse('booking:create_schedule')
            )
        elif(
            self.request.user.user_profile.user_type ==
            self.request.user.user_profile.USER_TYPE_INCHARGE
        ):
            return HttpResponseRedirect(
                reverse('common:index2')
            )
        else:
            return HttpResponseRedirect(
                reverse('common:index')
            )

    def form_invalid(self, form):
        return super(LoginView, self).form_invalid(form)


class LogoutView(RedirectView):

    def dispatch(self, request, *args, **kwargs):
        auth_logout(self.request)
        return super(LogoutView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(reverse('common:login'))


class RegisterView(FormView):
    form_class = auth_forms.UserCreationForm
    template_name = 'new_user.html'

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_authenticated():
            if (
                self.request.user.user_profile.user_type ==
                self.request.user.user_profile.USER_TYPE_MUNSHI
            ):
                return HttpResponseRedirect(
                    reverse('booking:create_schedule')
                )
            elif(
                self.request.user.user_profile.user_type ==
                self.request.user.user_profile.USER_TYPE_INCHARGE
            ):
                return HttpResponseRedirect(
                    reverse('common:index2')
                )
            else:
                return super(
                    RegisterView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        with transaction.atomic():
            user = form.save()
            if user.user_profile:
                user.user_profile.password_view = self.request.POST.get(
                    'password1')
                user.user_profile.user_type = self.request.POST.get(
                    'user_type')
                user.user_profile.save()

            RouteUser.objects.create(
                route=Route.objects.get(name=self.request.POST.get('route_name')),
                user=user
            )

            return HttpResponseRedirect(reverse('common:list_user'))

    def form_invalid(self, form):
        print form.errors
        print "_________________________________"
        print "_________________________________"
        print "_________________________________"
        print "_________________________________"
        print "_________________________________"
        print "_________________________________"
        return super(RegisterView, self).form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(RegisterView, self).get_context_data(**kwargs)
        context.update({
            'routes': Route.objects.all().order_by('name')
        })
        return context


class HomePageView(TemplateView):
    template_name = 'index.html'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect(reverse('common:login'))
        elif (
                    self.request.user.user_profile.user_type ==
                    self.request.user.user_profile.USER_TYPE_MUNSHI
        ):
            return HttpResponseRedirect(
                reverse('booking:create_schedule')
            )
        elif(
            self.request.user.user_profile.user_type ==
            self.request.user.user_profile.USER_TYPE_INCHARGE
        ):
            return HttpResponseRedirect(
                reverse('common:index2')
            )

        else:
            return super(
                HomePageView,self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context= super(HomePageView, self).get_context_data(**kwargs)
        return context


class InchargePageView(TemplateView):
    template_name = 'reports/incharge_reports.html'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect(reverse('common:login'))
        elif (
                    self.request.user.user_profile.user_type ==
                    self.request.user.user_profile.USER_TYPE_MUNSHI
        ):
            return HttpResponseRedirect(
                reverse('booking:create_schedule')
            )

        else:
            return super(
                InchargePageView,self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(InchargePageView, self).get_context_data(**kwargs)
        today = timezone.now().date()
        starting_date = timezone.datetime(
            settings.STARTED_YEAR, settings.STARTED_MONTH,
            settings.STARTED_DAY
        )

        days_difference = today - starting_date.date()
        total_days = days_difference.days
        result_data = []
        last_amount = 0
        for day in range(total_days):
            departure_date = starting_date + relativedelta(days=day)
            departure_bookings = Booking.objects.filter(
                route=self.request.user.user_route.route,
                departure__year=departure_date.year,
                departure__month=departure_date.month,
                departure__day=departure_date.day,
                has_payment=True
            )
            total_departure_bookings = departure_bookings.count()
            if departure_bookings.exists():
                departure_amount = (
                    departure_bookings.aggregate(Sum('total_amount')))
                departure_amount = float(
                    departure_amount.get('total_amount__sum'))

                departure_discount = (
                    departure_bookings.aggregate(Sum('discount')))
                departure_discount = float(
                    departure_discount.get('discount__sum'))

                total_departure_amount = departure_amount - departure_discount
            else:
                total_departure_amount = 0

            last_amount = last_amount + total_departure_amount

            route_bookings = Booking.objects.filter(
                route=self.request.user.user_route.route,
                has_payment=True
            )
            total_route_bookings = route_bookings.count()
            if route_bookings.exists():
                route_amount = route_bookings.aggregate(Sum('total_amount'))
                route_amount = float(route_amount.get('total_amount__sum'))

                route_discount = route_bookings.aggregate(Sum('discount'))
                route_discount = float(route_discount.get('discount__sum'))

                total_route_amount = route_amount - route_discount
            else:
                total_route_amount = 0

            advance_amount = total_route_amount - last_amount

            data = {}
            data.update({
                'date': departure_date.date(),
                'total_departure_amount': total_departure_amount,
                'advance_amount': advance_amount,
                'total_departure_bookings': total_departure_bookings,
                'total_route_bookings': total_route_bookings,
                'total_route_amount': total_route_amount
            })
            result_data.append(data)

        objects = Paginator(result_data, 20)

        context.update({
            'result_data': result_data,
            'objects': objects
        })

        return context


class  UserTicketRecordsView(TemplateView):
    template_name = 'user_records/records.html'

    def get_context_data(self, **kwargs):
        context = super(UserTicketRecordsView, self).get_context_data(**kwargs)

        user = User.objects.get(id=self.kwargs.get('user_id'))
        ticket_records = UserTickerRecords.objects.filter(user=user)

        if self.kwargs.get('date'):
            logs_date = self.kwargs.get('date').split('-')
            year = logs_date[0]
            month = logs_date[1]
            day = logs_date[2]
            ticket_records = ticket_records.filter(
                record_date__day=day, record_date__month=month,
                record_date__year=year
            )
            context.update({
                'logs_date': self.kwargs.get('date')
            })
        else:
            ticket_records = ticket_records.filter(
                record_date__day=timezone.now().day,
                record_date__month=timezone.now().month,
                record_date__year=timezone.now().year
            )
            context.update({
                'today_date': timezone.now().strftime('%Y-%m-%d')
            })

        reserved_tickets = ticket_records.filter(
                ticket_status=UserTickerRecords.STATUS_RESERVED
            )

        if ticket_records.exists():
            total_amount = ticket_records.aggregate(Sum('ticket_amount'))
            total_amount = float(total_amount.get('ticket_amount__sum'))

            total_quantity = ticket_records.aggregate(Sum('ticket_quantity'))
            total_quantity = total_quantity.get('ticket_quantity__sum')
        else:
            total_amount = 0
            total_quantity = 0

        if reserved_tickets.exists():
            reserved_amount = reserved_tickets.aggregate(Sum('ticket_amount'))
            reserved_amount = float(reserved_amount.get('ticket_amount__sum'))

            reserved_quantity = reserved_tickets.aggregate(
                Sum('ticket_quantity'))
            reserved_quantity = reserved_quantity.get('ticket_quantity__sum')
        else:
            reserved_amount = 0
            reserved_quantity = 0

        cancelled_tickets = ticket_records.filter(
                ticket_status=UserTickerRecords.STATUS_CANCELLED
            )

        if cancelled_tickets.exists():
            cancelled_amount = cancelled_tickets.aggregate(Sum('ticket_amount'))
            cancelled_amount = float(cancelled_amount.get('ticket_amount__sum'))

            cancelled_quantity = cancelled_tickets.aggregate(
                Sum('ticket_quantity'))
            cancelled_quantity = cancelled_quantity.get('ticket_quantity__sum')
        else:
            cancelled_amount = 0
            cancelled_quantity = 0

        total_amount = reserved_amount - cancelled_amount

        context.update({
            'ticket_records': ticket_records,
            'user_id': self.kwargs.get('user_id'),
            'user': user,
            'total_amount': total_amount,
            'total_quantity': total_quantity,
            'reserved_amount': reserved_amount,
            'reserved_quantity': reserved_quantity,
            'cancelled_amount': cancelled_amount,
            'cancelled_quantity': cancelled_quantity
        })
        return context


class DailyReportsView(TemplateView):
    template_name = 'reports/daily.html'

    def get_context_data(self, **kwargs):
        context = super(DailyReportsView, self).get_context_data(**kwargs)
        data_result = []
        for day in range(90):
            data = {}
            departure_day = timezone.now() - relativedelta(days=day)
            departure_booking = Booking.objects.filter(
                departure__year=departure_day.year,
                departure__month=departure_day.month,
                departure__day=departure_day.day,
                has_payment=True,
                departure_timing__daewoo_type=DepartureTiming.TYPE_2X1
            )
            if departure_booking.exists():
                departure_amount = departure_booking.aggregate(
                    Sum('total_amount'))
                departure_amount = float(
                    departure_amount.get('total_amount__sum'))

                departure_discount = (
                    departure_booking.aggregate(Sum('discount')))
                departure_discount = float(
                    departure_discount.get('discount__sum'))
                total_departure_amount_2x1 = (
                    departure_amount - departure_discount
                )
            else:
                total_departure_amount_2x1 = 0

            departure_booking = Booking.objects.filter(
                departure__year=departure_day.year,
                departure__month=departure_day.month,
                departure__day=departure_day.day,
                has_payment=True,
                departure_timing__daewoo_type=DepartureTiming.TYPE_2X2
            )
            if departure_booking.exists():
                departure_amount = departure_booking.aggregate(
                    Sum('total_amount'))
                departure_amount = float(
                    departure_amount.get('total_amount__sum'))

                departure_discount = (
                    departure_booking.aggregate(Sum('discount')))
                departure_discount = float(
                    departure_discount.get('discount__sum'))
                total_departure_amount_2x2 = (
                    departure_amount - departure_discount
                )
            else:
                total_departure_amount_2x2 = 0

            departure_booking = Booking.objects.filter(
                departure__year=departure_day.year,
                departure__month=departure_day.month,
                departure__day=departure_day.day,
                has_payment=True,
                departure_timing__daewoo_type=DepartureTiming.TYPE_HINO
            )
            if departure_booking.exists():
                departure_amount = departure_booking.aggregate(
                    Sum('total_amount'))
                departure_amount = float(
                    departure_amount.get('total_amount__sum'))

                departure_discount = (
                    departure_booking.aggregate(Sum('discount')))
                departure_discount = float(
                    departure_discount.get('discount__sum'))
                total_departure_amount_hino = (
                    departure_amount - departure_discount
                )
            else:
                total_departure_amount_hino = 0

            departure_booking = Booking.objects.filter(
                departure__year=departure_day.year,
                departure__month=departure_day.month,
                departure__day=departure_day.day,
                has_payment=True,
            )
            if departure_booking.exists():
                departure_amount = departure_booking.aggregate(
                    Sum('total_amount'))
                departure_amount = float(
                    departure_amount.get('total_amount__sum'))

                departure_discount = (
                    departure_booking.aggregate(Sum('discount')))
                departure_discount = float(
                    departure_discount.get('discount__sum'))
                total_departure_amount = (
                    departure_amount - departure_discount
                )
            else:
                total_departure_amount = 0

            data.update({
                'total_departure_amount_2x1': total_departure_amount_2x1,
                'total_departure_amount_2x2': total_departure_amount_2x2,
                'total_departure_amount_hino': total_departure_amount_hino,
                'total_departure_amount': total_departure_amount,
                'date': departure_day.date()
            })
            data_result.append(data)

        context.update({
            'results': data_result
        })

        return context


class MonthlyReportsView(TemplateView):
    template_name = 'reports/monthly.html'

    def get_context_data(self, **kwargs):
        context = super(MonthlyReportsView, self).get_context_data(**kwargs)
        data_result = []
        for month in range(60):
            data = {}
            departure_month = timezone.now() - relativedelta(months=month)
            month_range = monthrange(
                departure_month.year, departure_month.month
            )
            start_month = datetime.datetime(
                departure_month.year, departure_month.month, 1)

            end_month = datetime.datetime(
                departure_month.year, departure_month.month, month_range[1]
            )

            departure_booking = Booking.objects.filter(
                departure__gt=start_month,
                departure__lt=end_month.replace(
                    hour=23, minute=59, second=59),
                has_payment=True,
                departure_timing__daewoo_type=DepartureTiming.TYPE_2X1
            )
            if departure_booking.exists():
                departure_amount = departure_booking.aggregate(
                    Sum('total_amount'))
                departure_amount = float(
                    departure_amount.get('total_amount__sum'))

                departure_discount = (
                    departure_booking.aggregate(Sum('discount')))
                departure_discount = float(
                    departure_discount.get('discount__sum'))
                total_departure_amount_2x1 = (
                    departure_amount - departure_discount
                )
            else:
                total_departure_amount_2x1 = 0

            departure_booking = Booking.objects.filter(
                departure__gt=start_month,
                departure__lt=end_month.replace(
                    hour=23, minute=59, second=59),
                has_payment=True,
                departure_timing__daewoo_type=DepartureTiming.TYPE_2X2
            )
            if departure_booking.exists():
                departure_amount = departure_booking.aggregate(
                    Sum('total_amount'))
                departure_amount = float(
                    departure_amount.get('total_amount__sum'))

                departure_discount = (
                    departure_booking.aggregate(Sum('discount')))
                departure_discount = float(
                    departure_discount.get('discount__sum'))
                total_departure_amount_2x2 = (
                    departure_amount - departure_discount
                )
            else:
                total_departure_amount_2x2 = 0

            departure_booking = Booking.objects.filter(
                departure__gt=start_month,
                departure__lt=end_month.replace(
                    hour=23, minute=59, second=59),
                has_payment=True,
                departure_timing__daewoo_type=DepartureTiming.TYPE_HINO
            )
            if departure_booking.exists():
                departure_amount = departure_booking.aggregate(
                    Sum('total_amount'))
                departure_amount = float(
                    departure_amount.get('total_amount__sum'))

                departure_discount = (
                    departure_booking.aggregate(Sum('discount')))
                departure_discount = float(
                    departure_discount.get('discount__sum'))
                total_departure_amount_hino = (
                    departure_amount - departure_discount
                )
            else:
                total_departure_amount_hino = 0

            departure_booking = Booking.objects.filter(
                departure__gt=start_month,
                departure__lt=end_month.replace(
                    hour=23, minute=59, second=59),
                has_payment=True,
            )
            if departure_booking.exists():
                departure_amount = departure_booking.aggregate(
                    Sum('total_amount'))
                departure_amount = float(
                    departure_amount.get('total_amount__sum'))

                departure_discount = (
                    departure_booking.aggregate(Sum('discount')))
                departure_discount = float(
                    departure_discount.get('discount__sum'))
                total_departure_amount = (
                    departure_amount - departure_discount
                )
            else:
                total_departure_amount = 0

            data.update({
                'total_departure_amount_2x1': total_departure_amount_2x1,
                'total_departure_amount_2x2': total_departure_amount_2x2,
                'total_departure_amount_hino': total_departure_amount_hino,
                'total_departure_amount': total_departure_amount,
                'date': start_month.strftime('%B, %Y')
            })

            data_result.append(data)

        context.update({
            'results': data_result
        })
        return context
